from graphene import Argument, Field, String


# Arguments to query by unique value
# Values: are unique columns and primary keys (from model)
def createSQLAlchemyFieldArguments(schema):
    return {
        "column": Argument(
            type=schema.enum_unique_columns,
            description="Column to select from",
            required=True),
        "value": Argument(
            type=String,
            description="Value to search for",
            required=True)
    }


# Resolver to process arguments
def SQLAlchemyFieldResolver(schema):
    def resolver(self, info, column, value):
        query = schema.get_query(info)
        query = query.filter(schema.columns[column].column == value)
        return query.one_or_none()
    return resolver


# SQLA Field type
def SQLAlchemyField(schema):
    return Field(
        schema,
        args=createSQLAlchemyFieldArguments(schema),
        resolver=SQLAlchemyFieldResolver(schema))
