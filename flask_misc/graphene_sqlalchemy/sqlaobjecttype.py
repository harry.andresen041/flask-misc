from graphene import Dynamic, Field, List, Enum
from graphene.utils.subclass_with_meta import SubclassWithMeta_Meta
from graphene_sqlalchemy import (
    SQLAlchemyObjectType as SQLAlchemyObjectTypeBase)
from sqlalchemy import inspect
from sqlalchemy.orm import interfaces

from .sqlafield import createSQLAlchemyFieldArguments, SQLAlchemyFieldResolver
from .sqlalist import createSQLAlchemyListArguments, SQLAlchemyListResolver


# Object to hold meta regarding a sql alchemy object
#   ID - Unique id for column   Required for Enum field
#   Name - Column name          Required for Name
#   Column - Column obj         Required for order_by
#   Unique - Is unique column   Required for query for id
class _SQLAColumn(object):
    def __init__(self, id, name, column, unique):
        self.id = id
        self.name = name
        self.column = column
        self.unique = unique


# Metaclass to add attributes for custom Field resolvers
#   Recreate the dynamic type (for relationships) to use custom
#       arguments/resolvers defined above
class SQLAlchemyObjectTypeMeta(type):
    def __init__(cls, name, bases, nmspc):
        if name == "SQLAlchemyObjectType":
            return

        cls.columns = {}
        column_id_count = 0

        for name, column in inspect(cls._meta.model).columns.items():
            if name in cls._meta.fields.keys():
                unique = (column.unique or column.primary_key)
                cls.columns[name] = _SQLAColumn(column_id_count, name, column,
                                                unique)
                column_id_count += 1

        cls.enum_columns = Enum(
            "{}_columns".format(cls.__name__),
            [(column.name, column.id) for column in cls.columns.values()])

        cls.enum_unique_columns = Enum(
            "{}_uniquecolumns".format(cls.__name__),
            [(column.name, column.id) for column in cls.columns.values()
                if column.unique])

        for name, field in cls._meta.fields.items():
            if type(field) is Dynamic:
                def dynamic_type():
                    relationship = inspect(cls._meta.model).relationships[name]
                    schema_type = cls._meta.registry.get_type_for_model(
                        relationship.mapper.entity)
                    if not schema_type:
                        return None
                    if relationship.direction is interfaces.MANYTOONE\
                            or not relationship.uselist:
                        return Field(
                            type=schema_type,
                            args=createSQLAlchemyFieldArguments(schema_type),
                            resolver=SQLAlchemyFieldResolver(schema_type)),
                    else:
                        return List(
                            of_type=schema_type,
                            args=createSQLAlchemyListArguments(schema_type),
                            resolver=SQLAlchemyListResolver(schema_type))

                cls._meta.fields[name] = Dynamic(dynamic_type)


class _CombinedMeta(SQLAlchemyObjectTypeMeta, SubclassWithMeta_Meta):
    pass


class SQLAlchemyObjectType(SQLAlchemyObjectTypeBase, metaclass=_CombinedMeta):
    class Meta:
        abstract = True
