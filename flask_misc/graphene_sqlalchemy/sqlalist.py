from graphene import Argument, Enum, Int, List, String


# SQLA Order Types
class SQLAOrder(Enum):
    ASC = 0
    DESC = 1


# Arguments to query by SQLA functions
# Values: .limit(int) (limit), .filter(col=val) (filter),
#           .order_by(col=asc/desc) (order)
def createSQLAlchemyListArguments(schema):
    return {
        "limit": Argument(
            type=Int,
            description="Limit list to x items"),
        "filter": Argument(
            type=List(String),
            description="Value to filter by"),
        "filter_by": Argument(
            type=List(schema.enum_columns),
            description="Column to filter by"),
        "order": Argument(
            type=List(SQLAOrder),
            description="Order query by"),
        "order_by": Argument(
            type=List(schema.enum_columns),
            description="Columns to order by"),
        "per_page": Argument(
            type=Int,
            description="Page size"),
        "page": Argument(
            type=Int,
            description="Page number")
    }


# Resolver to process arguments
def SQLAlchemyListResolver(schema):
    def resolver(self, info, **kwargs):
        query = schema.get_query(info)
        columns = schema.columns
        enum_columns = schema.enum_columns

        for column, value in zip(
                kwargs.get("filter_by", []), kwargs.get("filter", [])):
            query = query.filter(
                columns[enum_columns.get(column).name].column == value)

        for column, value in zip(
                kwargs.get("order_by", []), kwargs.get("order", [])):
            query = query.order_by(getattr(
                columns[enum_columns.get(column).name],
                SQLAOrder.get(value).name))

        limit = kwargs.get("limit", None)
        if limit:
            query = query.limit(limit)

        per_page = kwargs.get("page", None)
        page = kwargs.get("page", None)
        if per_page and page:
            query = query.limit(per_page).offset((page - 1) * per_page)

        return query.all()
    return resolver


# SQLA List type
def SQLAlchemyList(schema):
    return List(
        schema,
        args=createSQLAlchemyListArguments(schema),
        resolver=SQLAlchemyListResolver(schema))
